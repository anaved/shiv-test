from django.conf.urls import patterns, include, url

from master.page import MyPage


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
urlpatterns = patterns('',
    # Examples:
    url(r'^$', MyPage.as_view(), name='home'),
#     (r'^async/', include('shiv.urls')),
    # url(r'^shiv_test/', include('shiv_test.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)

#!/usr/bin/env python
#
#       media.py
#       
from shiv.media import *

class PLandingMedia(Media):
    css = ["base.css",'products/PLanding.css']
    js = ["http://code.jquery.com/jquery-1.10.1.min.js",'products/PLanding.js']


###Landing Page
class HeaderBoxMedia(Media):
    template ='BHeader.html' 
    css = ['BHeader.css']
    js = ['BHeader.js']

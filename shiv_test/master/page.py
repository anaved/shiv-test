# Create your views here.
from braces.views import CsrfExemptMixin
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import render
from django.views.generic.base import View, ContextMixin, TemplateResponseMixin, \
    TemplateView
import urllib

class MyPage(TemplateResponseMixin, ContextMixin, View):
    template_name = 'NewFile.html'
    request_validator=None
    def title(self,request,*args,**kwargs):
        return "This is page title def"
    def get_request_validator(self):
        return self.request_validator

    def get_context_data(self,request,*args,**kwargs):
        context = super(MyPage, self).get_context_data(**kwargs)
        context.update(self.shiv_context(request, *args, **kwargs))
        return context        
        
    def get(self, request,*args,**kwargs):
        context = self.get_context_data(request,*args,**kwargs)
        return self.render_to_response(context)
        
    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)
    
#    TODO:implement this, this can be called from all other methods
#     def execute_callable(self,obj,request, * args, ** kwargs):
#         title=getattr(self, 'title', '')
#         return title(request,*args,**kwargs) if callable(title) else title
    
    def get_title(self,request, * args, ** kwargs):
        title=getattr(self, 'title', '')
        return title(request,*args,**kwargs) if callable(title) else title
    
    def get_header(self,request, * args, ** kwargs):
        header=getattr(self, 'header', '')
        return header(request,*args,**kwargs) if callable(header) else header

    def get_boxes(self,request, * args, ** kwargs):
        obj=getattr(self, 'boxes', [])
        return [e(request, * args, ** kwargs)  for e in obj]        

    def get_footer(self,request, * args, ** kwargs):
        obj=getattr(self, 'footer', '')
        return obj(request,*args,**kwargs) if callable(obj) else obj

    def get_css(self,request, * args, ** kwargs):
        #TODO: Create default breadcrumbs based upon url
        obj=getattr(self, 'css', [])
        return obj(request,*args,**kwargs) if callable(obj) else obj

    def get_js(self,request, * args, ** kwargs):
        #TODO: Create default breadcrumbs based upon url
        obj=getattr(self, 'js', [])
        return obj(request,*args,**kwargs) if callable(obj) else obj
 
    def get_breadcrumb(self,request, * args, ** kwargs):
        #TODO: Create default breadcrumbs based upon url
        obj=getattr(self, 'breadcrumb', '')
        return obj(request,*args,**kwargs) if callable(obj) else obj
 
    def get_meta_tags(self,request, * args, ** kwargs):
        #TODO: This calls a meta tags related box
        obj=getattr(self, 'meta_tags', '')
        return obj(request,*args,**kwargs) if callable(obj) else obj
    
    def get_page_type(self,request, * args, ** kwargs):
        obj=getattr(self, 'page_type', '')
        return obj(request,*args,**kwargs) if callable(obj) else obj
           
    def shiv_context(self,request,*args,**kwargs):
        extra_context={}       
        extra_context['title'] = self.get_title(request, * args, ** kwargs)
        extra_context['header'] = self.get_header(request, * args, ** kwargs)
        extra_context['boxes'] = self.get_boxes(request, * args, ** kwargs)
        extra_context['footer'] = self.get_footer(request, * args, ** kwargs)
        extra_context['css'] = self.get_css(request, * args, ** kwargs)
        extra_context['js'] = self.get_js(request, * args, ** kwargs)
        extra_context['breadcrumb'] = self.get_breadcrumb(request, * args, ** kwargs)
        extra_context['meta_tags'] = self.get_meta_tags(request, * args, ** kwargs)
        extra_context['page_type'] = self.get_page_type(request, * args, ** kwargs)
        return extra_context         
        
    def dispatch(self, request, *args, **kwargs):
        if self.get_request_validator():
            validator = self.get_request_validator()(request, args, kwargs)            
            if not validator:
                raise Http404            
            self.obj = validator
        if not hasattr(self, 'context'):            
            self.context={}        
        return super(MyPage, self).dispatch(
            request, *args, **kwargs)    
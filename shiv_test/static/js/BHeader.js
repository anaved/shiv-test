/**
 * 
 */
$(function(){
	
	$.each(CITIES,function(i,v){
		var li='<li class="city-element replace">'+v+'</li>';
		$(li).insertAfter('.locate-me');
	});
	
	$.each(($.cookie('location_history'))?JSON.parse($.cookie('location_history')):[] ,function(i,v){
		var li='<li class="recent-location-element replace">'+v+'</li>';
		$(li).insertAfter('.recent-locations');
		
	});
	$('#explore-location').on('click',function(){
		var that= $(this);
		that.attr('placeholder','Search Location Here');
	});
	
	$('#location-selection-list .replace').on('click',function(){
		val=$(this).html();
		$('.set-loc').hide();
		$('#explore-location').val(val);
		$('#explore-location').attr('data-old',val);
		check_address();
		
	});
	
	$('#explore-location').on("change keyup paste", function(){		
		var that=$(this);
		var val=that.val();
		var old_val=that.attr('data-old');
		if (val!=old_val){
			$('.set-loc').show();
		}else{
			$('.set-loc').hide();
		}
	});	
	
	$('.set-loc').on('click',function(){
			check_address();		
			return false;
	});
	

	$('#location-select').on('submit',function(){		  
		  	return check_address();		  
	});
	
	$('.locate-me').on('click',function(){
		return locate_me();		  
});
	
});
function locate_me()
{
	
	var currPos = navigator.geolocation.getCurrentPosition(function(position) {
		
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
		result= call_geocode_lat_lng(lat,lng);
		return geocode_handler(result);		
	});
	
}



function unique_array(x){
	var	y = x.reverse().filter(function (e, i, x) {
	    return x.indexOf(e, i+1) === -1;
	}).reverse();
	return y;
}

function check_address(){	
	var result= call_geocode_address($('#explore-location').val());	
	return geocode_handler(result);
}

function geocode_handler(result){
	if (result){
		var faddress=result.results[0].formatted_address
		var n=faddress.split(',');		
		var name=(n.length >1)? n[0]+','+n[1] : n[0];		
		$.cookie('location', name);
		$.cookie('latitude', name);
		$.cookie('longitude', name);		
		var loc_hist=  $.cookie('location_history') ? JSON.parse($.cookie('location_history')) : [];				
		loc_hist.push(name);
		loc_hist=unique_array(loc_hist);
		if (loc_hist.length >5){			
			loc_hist =loc_hist.slice(loc_hist.length-5);	
		}				
		$.cookie('location_history',JSON.stringify(loc_hist));		
		
//		TODO ADD LAT LNG TO FORM FIELDS
		$('#explore-location').val(name);
		$('#lat').val(name);
		$('#lng').val(name);
		$('#explore-location').attr('data-old',name);
	   $('.loc-message').attr('data-type','success');
	   $('.loc-message').html('Location set to '+faddress);
		return true;
		}
	else{
		  $('.loc-message').attr('data-type','failure');
		  $('.loc-message').html('Location not found.');	
		return false;
	}	
}

function call_geocode_address(address){	
	var url="http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address="+address
	var result=null;
	$.ajax({
		url:url,
		success:function(data) {
			if (data.status=='OK'){
	         result = data; 
			}
	      },
		async: false,
	});
	return result;
}

function call_geocode_lat_lng(lat,lng){	
	var url="http://maps.googleapis.com/maps/api/geocode/json?sensor=true&latlng="+lat+","+lng;
	var result=null;
	$.ajax({
		url:url,
		success:function(data) {
			if (data.status=='OK'){
	         result = data; 
			}
	      },
		async: false,
	});
	return result;
}
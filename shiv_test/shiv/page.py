#!/usr/bin/env python
#
#       page.py
#       
#       Copyright 2010 yousuf <yousuf@postocode53.com>
#
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import AnonymousUser
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views.generic.base import View
from shiv.auth import login_required as lr
from shiv.box import NullBox
from shiv.container import Container
from shiv.settings import JS_MINIFY, CSS_MINIFY, MEDIA_ROOT, PROJECT_PATH, DEBUG, \
    JS_VERSION, CSS_VERSION
from shiv.utils.cssmin import cssmin
from shiv.utils.jsmin import jsmin
import inspect
import json
import logging
import operator
import os        
class PageMeta(type):
    def __init__(cls, name, bases, dict):
        for b in bases:
            if hasattr(b, '_registry'):
                b._registry[name] = cls
                cls.page_id = name                
                break
        return type.__init__(cls, name, bases, dict)
        
class Page(Container,View):
    __metaclass__ = PageMeta
    _registry = {}
    media = None
    boxes=[NullBox,]
    def __init__(self, login_required=True, login_url=None, permission=None):
        self.module = self.__module__.split('.')[-2]
        self.context = {}

       
    def __call__(self, request, *args, ** kwargs):
#         meta_log = dict([(e, request.META.get(e, 'N/A')) for e in META_LOGGER])        
#         log_info = {
#         'USER':str(request.user),
#         'PATH':request.path,
#         'URL':request.build_absolute_uri(),
#         'PARAMS' :request.REQUEST.items(),
#         'META':meta_log,
#         'COOKIES': request.COOKIES
#         }        
#         LOGGER.info(json.dumps(log_info))

        self.boxes = self.boxes        
        return self.view(request, *args, ** kwargs)
     
    def show(self, request, * args, ** kwargs):        
        if not hasattr(self, 'css'):
            self.css, self.js = self.get_css_js()        
        self.context['css'] = self.css
        self.context['title'] = getattr(self, 'page_title', '')
        #this can have meta fb script and all other schebang which which goes in head section        
        self.context['head_content'] =  self.head_content(request, module=self.module).show() if hasattr(self, 'head_content')  else '' 
        self.context['header'] = self.header(request, module=self.module, * args, ** kwargs).show() if hasattr(self, 'header') else ''
        self.context['boxes'] = [e(request, module=self.module, * args, ** kwargs).show()  for e in self.boxes]        
        self.context['footer'] =  self.footer(request, module=self.module,* args, ** kwargs).show() if hasattr(self, 'footer') else ''
        self.context['js'] = self.js        
        self.context['module'] = self.module
        return HttpResponse(super(Page, self).show(self.template_name, request,  None))

    def unique_list(self, l):
        ulist = []
        [ulist.append(x) for x in l if x not in ulist]
        return ulist

    def get_css_js(self):
        css = []
        js = []
        if type(self.media) == type(''):
            module, media = self.media.rsplit('.', 1)
            module = __import__(module, fromlist=[module])
            media = getattr(module, media)
            css = [e.startswith('http') and e or media.css_prefix + e for e in media.css]
            js = [e.startswith('http') and e or media.js_prefix + e for e in media.js]

        for e in self.__dict__.values():            
            if isinstance(e, list):
                for b in e:
                    if inspect.isclass(b) and (isinstance(b, Container) or issubclass(b, Container)):
                        css.extend(b.get_css())
                        js.extend(b.get_js())
                continue

            if inspect.isclass(e) and (isinstance(e, Container) or issubclass(e, Container)):
                css.extend(e.get_css())
                js.extend(e.get_js())

        # js minification function
        if JS_MINIFY:
            js = minify_js(self.unique_list(js), "%s_%s_" % (self.module, self.page_id))

        # css minification function
        if CSS_MINIFY:
            css = minify_css(self.unique_list(css), "%s_%s_" % (self.module, self.page_id))

        return [x[:-1] if x.endswith('/') else x for x in self.unique_list(css)], [x[:-1] if x.endswith('/') else x for x in self.unique_list(js)]
        
def minify_js(js, page_id):
        xs = []
        jsdata = ''
        exists = False
        if os.path.exists(MEDIA_ROOT + '/js/minified/' + page_id + str(JS_VERSION) + '.js'):
            exists = True
        for e in js:
            if e.startswith('http'):
                xs.append(e)
                continue
            if exists:continue
            try:
                f = open(PROJECT_PATH + e)                                    
                jsdata += '\n//' + e + '\n' + f.read() + '\n'
                f.close()
            except:pass
        if not exists:
            of = open(MEDIA_ROOT + '/js/minified/' + page_id + str(JS_VERSION) + '.js', 'w')
            if DEBUG:
                of.write(jsmin(jsdata))
            else:
                of.write(jsmin(jsdata))
            of.close()
        js = xs + ['/static/js/minified/' + page_id + str(JS_VERSION) + '.js']
        return js
    
def minify_css(css, page_id):        
        xs = []
        cssdata = ''
        exists = False
        if os.path.exists(MEDIA_ROOT + '/css/minified/' + page_id + str(CSS_VERSION) + '.css'):
            exists = True
        for e in css:
            if e.startswith('http'):
                xs.append(e)                
                continue
            if exists:continue
            try:
                f = open(PROJECT_PATH + e)                                
                cssdata += '\n/*' + e + '*/\n' + f.read()
            except:pass
        if not exists:
            of = open(MEDIA_ROOT + '/css/minified/' + page_id + str(CSS_VERSION) + '.css', 'w')
            of.write(cssmin("".join(cssdata)))
            of.close()
        css = xs + ['/static/css/minified/' + page_id + str(CSS_VERSION) + '.css']
        return css


def redirect_to_internal(url, index=1):
    # NOTE check university name removal part
    url_list = url.split('/')
    url_list.pop(index)
    url_list.pop(index)
    url = "/".join(url_list)
    return HttpResponseRedirect(url)

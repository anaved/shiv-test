#!/usr/bin/env python
#
#       box.py
#       

from django.template.base import TemplateDoesNotExist
from django.template.loader import get_template
from shiv import data_methods as dm
from shiv.container import Container
from shiv.media import NullBoxMedia
from shiv.settings import TEMPLATE_CACHING
from shiv.tab import NullTab



#So that it all happens only once and not at every instance
#TODO repaced prints with loggers
#TODO log error, and move on with default media 
class BoxMeta(type):
    def __init__(cls, name, bases, dict):
        for b in bases:
            if hasattr(b, '_registry'):
                b._registry[name] = cls
                cls.box_id = name
                module = __import__(dict['__module__'].rsplit('.', 1)[0] + '.media', fromlist=[dict['__module__'].rsplit('.', 1)[0]])                
                try:
                    cls._media = getattr(module, name + 'Media')()
                except AttributeError: 
                    #TODO add logger
                    cls._media=NullBoxMedia()                                  
                cls._media.css = [e.startswith('http') and e or cls._media.css_prefix + e for e in cls._media.css]
                cls._media.js = [e.startswith('http') and e or cls._media.js_prefix + e for e in cls._media.js]
                for elem in dict.get('_tab_class',[]):
                    cls._media.extra_css = cls._media.extra_css + elem._media.css + elem._media.extra_css
                    cls._media.extra_js = cls._media.extra_js + elem._media.js + elem._media.extra_js
                if TEMPLATE_CACHING:
                    cls._media.template = get_template(cls._media.template)
                break
        return type.__init__(cls, name, bases, dict)
#view        
class Box(Container):
    __metaclass__ = BoxMeta
    _registry = {}    
    title='' 
        
    def __init__(self,request, *args, **kwargs):         
        self.context = {
                        'id': self.box_id,
                        'title' : self.title,                        
                        'images': self._media.images}
        self.request = request

    def show(self, template=None,):                               
        if hasattr(self,'tabs'):
            self.context['tabs'] = [t.show() for t in self.tabs]        
        return super(Box, self).show(template, self.request, self.context)

    @classmethod
    def get_css(cls):
        return cls._media.css + cls._media.extra_css

    @classmethod
    def get_js(cls):
        return cls._media.js + cls._media.extra_js

class NullBox(Box):
    _tab_class = [NullTab,]

            

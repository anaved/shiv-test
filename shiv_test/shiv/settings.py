'''
Created on Sep 21, 2013

@author: Naved
'''
from django.conf import settings
import random
shiv_settings=getattr(settings,'SHIV_SETTINGS','')
TEMPLATE_CACHING=getattr(shiv_settings,'TEMPLATE_CACHING',False)
JS_MINIFY=getattr(shiv_settings,'JS_MINIFY',False)
CSS_MINIFY=getattr(shiv_settings,'CSS_MINIFY',False)
JS_VERSION=getattr(shiv_settings,'JS_VERSION',random.randint(1,10))
CSS_VERSION=getattr(shiv_settings,'CSS_VERSION',random.randint(1,10))
CSS_MINIFY=getattr(shiv_settings,'CSS_MINIFY',False)
MEDIA_ROOT=getattr(settings,'MEDIA_ROOT','')
PROJECT_PATH=getattr(settings,'MEDIA_ROOT','')
DEBUG=getattr(settings,'DEBUG','')
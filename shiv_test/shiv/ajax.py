#-------------------------------------------------------------------------------
# Copyright (c) Requext LLC. 2013 .
# All rights reserved. This program and the accompanying materials
# are property of Requext LLC.
# 
# Contributors:
#     Naved - initial API and implementation
#-------------------------------------------------------------------------------
# To change this template, choose Tools | Templates
# and open the template in the editor.
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponse, HttpResponse
from django.views.generic.base import View
from shiv import settings as ss, LOG
from shiv.callback import Callback
from shiv.json_utils import JsonResponse
import json


__author__ = "naved"
__date__ = "$22 Jun, 2010 1:32:06 PM$"
cb = Callback()
def allowAnonymous(funct):
    try:    
        funct.__setattr__("allowAnonymous", True)
    except AttributeError as err:
        err.args += ('. Please put @staticmethod before @allowAnonymous',)         
        raise err
    return funct

def unique_list(l):
    ulist = []
    [ulist.append(x) for x in l if x not in ulist]
    return ulist

class AsyncView(View):
    
    def __init__(self, **kwargs):
        self.log = self._get_log()
        super(AsyncView, self).__init__()        
        
    def _get_log(self):
        return LOG
            
    def get(self, request):
        self.log.debug("Received GET : %s",request.GET.items())
        if not len(request.GET) or not request.GET.get('data_async'): return JsonResponse(json.dumps({'action':2, 'status':'error', 'content':'No Parameters Specified, require "data_async" for routing.'}))
        data_id = request.GET['data_async'].split('+')
        return self._responder(data_id,request)        

    def post(self, request):
        self.log.debug("Received POST : %s",request.POST.items())
        if not len(request.POST) or not request.POST.get('data_async'): return JsonResponse(json.dumps({'action':2, 'status':'error', 'content':'No Parameters Specified, require "data_async" for routing.'}))
        data_id = request.POST['data_async'].split('+')
        return self._responder(data_id,request)        
    
    def _responder(self,data_id,request):
        func = cb._registry[data_id[0]]
        if func.func_dict.get("allowAnonymous", None) or request.user.is_authenticated():
            return func(request)
        else:
            return JsonResponse(json.dumps({'action':5, 'status':'error', 'content':'User Not Authorized'}))
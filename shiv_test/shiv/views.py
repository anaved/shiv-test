#!/usr/bin/env python
#
#       box.py
#       

from django.core.exceptions import ImproperlyConfigured
from django.http.response import Http404
from django.template.base import TemplateDoesNotExist
from django.template.loader import get_template
from django.views.generic.base import View
from shiv import data_methods as dm
from shiv.container import Container
from shiv.media import NullBoxMedia
from shiv.settings import TEMPLATE_CACHING
from shiv.tab import NullTab



#So that it all happens only once and not at every instance
#TODO repaced prints with loggers
#TODO log error, and move on with default media 
class BoxMeta(type):
    def __init__(cls, name, bases, dict):
        for b in bases:
            if hasattr(b, '_registry'):
                b._registry[name] = cls
                cls.box_id = name
                module = __import__(dict['__module__'].rsplit('.', 1)[0] + '.media', fromlist=[dict['__module__'].rsplit('.', 1)[0]])                
                try:
                    cls._media = getattr(module, name + 'Media')()
                except AttributeError: 
                    #TODO add logger
                    cls._media=NullBoxMedia()                                  
                cls._media.css = [e.startswith('http') and e or cls._media.css_prefix + e for e in cls._media.css]
                cls._media.js = [e.startswith('http') and e or cls._media.js_prefix + e for e in cls._media.js]
                for elem in dict.get('_tab_class',[]):
                    cls._media.extra_css = cls._media.extra_css + elem._media.css + elem._media.extra_css
                    cls._media.extra_js = cls._media.extra_js + elem._media.js + elem._media.extra_js
                if TEMPLATE_CACHING:
                    cls._media.template = get_template(cls._media.template)
                break
        return type.__init__(cls, name, bases, dict)
#view        
class Box(Container):
    __metaclass__ = BoxMeta
    _registry = {}    
    title='' 
        
    def __init__(self,request, *args, **kwargs):         
        self.context = {
                        'id': self.box_id,
                        'title' : self.title,                        
                        'images': self._media.images}
        self.request = request

    def show(self, template=None,):                               
        if hasattr(self,'tabs'):
            self.context['tabs'] = [t.show() for t in self.tabs]        
        return super(Box, self).show(template, self.request, self.context)

    @classmethod
    def get_css(cls):
        return cls._media.css + cls._media.extra_css

    @classmethod
    def get_js(cls):
        return cls._media.js + cls._media.extra_js

class NullBox(Box):
    _tab_class = [NullTab,]

            
class ShivPageMixin(object):
    """
    Mixin allows you to convert a view to a shiv page level result.
    """
    headline = None  # Default the headline to none
    media = None
    header=None
    boxes=[]
    footer=None
    request_validator=None
    def dispatch(self, request, *args, **kwargs):
        if self.request_validator:
            validator = self.request_validator(request, args, kwargs)            
            if not validator:
                raise Http404            
            self.obj = validator
        self.request = request
        return super(ShivPageMixin, self).dispatch(
            request, *args, **kwargs)    

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(request, *args, **kwargs)
        return self.render_to_response(context)
    
    def get_context_data(self, request, *args, **kwargs):
        kwargs = super(ShivPageMixin, self).get_context_data(**kwargs)
        # Update the existing context dict with the provided elements.
        kwargs.update({
                       "header": self.get_header(request, * args, ** kwargs),
                       "boxes": self.get_boxes(request, * args, ** kwargs),
                       "footer": self.get_footer(request, * args, ** kwargs),
                       "css": self.get_css(request, * args, ** kwargs),
                       "js": self.get_js(request, * args, ** kwargs),
                       "title": self.get_title(request, * args, ** kwargs),
                       "breadcrumb": self.get_breadcrumb(request, * args, ** kwargs),
                       "page_meta": self.get_page_meta(request, * args, ** kwargs),                       
                       "page_type": self.get_type(request, * args, ** kwargs),                                            
                       })
        return kwargs
    def get_boxes(self,request, * args, ** kwargs):
        pass

    def get_page_type(self,request, * args, ** kwargs):
        if self.type is None:  
        # If no headline was provided as a view
        # attribute and this method wasn't
        # overridden raise a configuration error.
            raise ImproperlyConfigured(
                "%(cls)s is missing a type. "
                "Define %(cls)s.type, or override "
                "%(cls)s.get_type()." % {"cls": self.__class__.__name__}
            )
        return self.footer

    def get_page_meta(self,request, * args, ** kwargs):
        pass

    def get_breadcrumb(self,request, * args, ** kwargs):
        pass

    def get_title(self,request, * args, ** kwargs):
        pass

    def get_css(self,request, * args, ** kwargs):
        pass

    def get_js(self,request, * args, ** kwargs):
        pass
    
    def get_footer(self,request, * args, ** kwargs):
        if self.footer is None:  
        # If no headline was provided as a view
        # attribute and this method wasn't
        # overridden raise a configuration error.
            raise ImproperlyConfigured(
                "%(cls)s is missing a footer. "
                "Define %(cls)s.footer, or override "
                "%(cls)s.get_footer()." % {"cls": self.__class__.__name__}
            )
        return self.footer
    def get_header(self,request, * args, ** kwargs):
        if self.header is None:  
        # If no headline was provided as a view
        # attribute and this method wasn't
        # overridden raise a configuration error.
            raise ImproperlyConfigured(
                "%(cls)s is missing a header. "
                "Define %(cls)s.header, or override "
                "%(cls)s.get_header()." % {"cls": self.__class__.__name__}
            )
        return self.header


class ShivPageView(View):
    """
    This view allows to create a shiv specific pages.
    """
    headline = None  # Default the headline to none
    media = None
    header=None
    boxes=[]
    footer=None
    request_validator=None
    def dispatch(self, request, *args, **kwargs):
        if self.request_validator:
            validator = self.request_validator(request, args, kwargs)            
            if not validator:
                raise Http404            
            self.obj = validator
        self.request = request
        return super(ShivPageMixin, self).dispatch(
            request, *args, **kwargs)    

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(request, *args, **kwargs)
        return self.render_to_response(context)
    
    def get_context_data(self, request, *args, **kwargs):
        kwargs = super(ShivPageMixin, self).get_context_data(**kwargs)
        # Update the existing context dict with the provided elements.
        kwargs.update({
                       "header": self.get_header(request, * args, ** kwargs),
                       "boxes": self.get_boxes(request, * args, ** kwargs),
                       "footer": self.get_footer(request, * args, ** kwargs),
                       "css": self.get_css(request, * args, ** kwargs),
                       "js": self.get_js(request, * args, ** kwargs),
                       "title": self.get_title(request, * args, ** kwargs),
                       "breadcrumb": self.get_breadcrumb(request, * args, ** kwargs),
                       "page_meta": self.get_page_meta(request, * args, ** kwargs),                       
                       "page_type": self.get_type(request, * args, ** kwargs),                                            
                       })
        return kwargs
    def get_boxes(self,request, * args, ** kwargs):
        pass

    def get_page_type(self,request, * args, ** kwargs):
        if self.type is None:  
        # If no headline was provided as a view
        # attribute and this method wasn't
        # overridden raise a configuration error.
            raise ImproperlyConfigured(
                "%(cls)s is missing a type. "
                "Define %(cls)s.type, or override "
                "%(cls)s.get_type()." % {"cls": self.__class__.__name__}
            )
        return self.footer

    def get_page_meta(self,request, * args, ** kwargs):
        pass

    def get_breadcrumb(self,request, * args, ** kwargs):
        pass

    def get_title(self,request, * args, ** kwargs):
        pass

    def get_css(self,request, * args, ** kwargs):
        pass

    def get_js(self,request, * args, ** kwargs):
        pass
    
    def get_footer(self,request, * args, ** kwargs):
        if self.footer is None:  
        # If no headline was provided as a view
        # attribute and this method wasn't
        # overridden raise a configuration error.
            raise ImproperlyConfigured(
                "%(cls)s is missing a footer. "
                "Define %(cls)s.footer, or override "
                "%(cls)s.get_footer()." % {"cls": self.__class__.__name__}
            )
        return self.footer
    def get_header(self,request, * args, ** kwargs):
        if self.header is None:  
        # If no headline was provided as a view
        # attribute and this method wasn't
        # overridden raise a configuration error.
            raise ImproperlyConfigured(
                "%(cls)s is missing a header. "
                "Define %(cls)s.header, or override "
                "%(cls)s.get_header()." % {"cls": self.__class__.__name__}
            )
        return self.header
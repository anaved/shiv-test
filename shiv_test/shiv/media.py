#!/usr/bin/env python
#
#       Media.py
#       

class Media(object):
    template = ''
    css = []
    js = []
    css_prefix = '/static/css/'
    js_prefix = '/static/js/'
    extra_css = []
    extra_js = []
    images = []


class NullBoxMedia(Media):
    template ='shiv/null.html' 
    css = []
    js = []
class NullTabMedia(Media):
    template = 'shiv/null.html'
    css = []
    js = []
class NullElementMedia(Media):
    template = 'shiv/null.html'
    css = []
    js = []
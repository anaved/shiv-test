#!/usr/bin/env python
#
#       tab.py
#       


from django.db.models.query import QuerySet
from django.template.loader import get_template
from shiv.container import Container
from shiv.element import NullElement
from shiv.nodes import Node
from shiv.paginator import Paginator
from shiv.settings import TEMPLATE_CACHING
from string import count



class TabMeta(type):
    def __init__(cls, name, bases, dict):        
        for b in bases:
            if hasattr(b, '_registry'):
                b._registry[name] = cls
                cls.tab_id = dict['__module__'].rsplit('.', 1)[0] + name
                module = __import__(dict['__module__'].rsplit('.', 1)[0] + '.media', fromlist=[dict['__module__'].rsplit('.', 1)[0]])
                cls._media = getattr(module, name + 'Media')()
                cls._media.css = [e.startswith('http') and e or cls._media.css_prefix + e for e in cls._media.css]
                cls._media.js = [e.startswith('http') and e or cls._media.js_prefix + e for e in cls._media.js]
                if '_element_class' in dict:
                    cls._media.extra_css = dict['_element_class']._media.css                
                    cls._media.extra_js = dict['_element_class']._media.js
                if TEMPLATE_CACHING:
                    cls._media.template = get_template(cls._media.template)
                break
        return type.__init__(cls, name, bases, dict)

#List view        
class Tab(Container):    
    __metaclass__ = TabMeta
    _registry = {}   
    title=''
    _element_class=NullElement
    def __init__(self, request , tab_client=None):        
        self.user = request.user
        self.client = tab_client
        self.context = {'elements': [], 
                        'title': Node(self.title),                  
                            'id': self.tab_id,
                            'images': self._media.images}
        self._prepare()        
        self.request = request

    def _prepare(self):
        self.ids = [self.client]
        
    def _get_elements(self, element_class, paginator):        
        return [ element_class(client=id, request=self.request,
                                ) for id in paginator.object_list]
        
    def show(self,template=None, start=1, count=10):                      
        element_class = self._element_class      
        paginator = Paginator(self.ids, count, request=self.request)
        ppage = paginator.page(start)
        self.context['elements'] = [e.show()\
                                for e in self._get_elements(element_class, ppage)]
        self.context['ppage'] = ppage            
        context = {}        
        context['html'] = super(Tab, self).show(template=template,
                                request=self.request,
                                context=self.context,
                                )
        context.update(self.context)
        if hasattr(self, 'client'):
            context['client'] = str(self.client)
        return context

class NullTab(Tab):
    _element_class = NullElement
    

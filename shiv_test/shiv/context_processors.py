'''
Created on 16-Sep-2013

@author: naved
'''
from django.contrib.sites.models import Site

def site(request):
        return {
            'site': Site.objects.get_current()
    }